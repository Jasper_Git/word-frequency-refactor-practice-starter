import java.util.*;
import java.util.function.Function;// 5. used import
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordFrequencyGame {
    private final String SPACE_MARK = "\\s+";

    private final String CALCULATE_ERROR_MSG = "Calculate Error";

    public String getResult(String inputStr) {
        String[] wordArr = inputStr.split(SPACE_MARK);

        try {
            List<Word> wordList = initialWordList(wordArr);
            Map<String, List<Word>> wordGroupByWord = getWordGroupByWord(wordList);
            List<Word> wordListWithWordFrequency = buildWordFrequencyList(wordGroupByWord);
            Collections.sort(wordListWithWordFrequency);
            return getResultString(wordListWithWordFrequency);
        } catch (Exception e) {
            return this.CALCULATE_ERROR_MSG;
        }
    }

    private String getResultString(List<Word> wordList) {
        return wordList.stream()
                .map(Word::getFullString)
                .collect(Collectors.joining("\n"));
//        StringJoiner joiner = new StringJoiner("\n");
//        wordList.forEach(word -> joiner.add(word.getFullString()));
//        return wordList.stream().map(word -> word.getFullString()+"\n").collect(Collectors.toList()).toString();
//        return joiner.toString();
    }

    private List<Word> buildWordFrequencyList(Map<String, List<Word>> wordGroupByWord) {
        return wordGroupByWord.entrySet().stream().map(entry -> new Word(entry.getKey(), entry.getValue().size())).collect(Collectors.toList());
    }

    private List<Word> initialWordList(String[] arr) {
        return Stream.of(arr).map(Word::new).collect(Collectors.toList());
    }

    private Map<String, List<Word>> getWordGroupByWord(List<Word> wordList) {
        return wordList.stream().collect(Collectors.groupingBy(Word::getValue));
    }
}
