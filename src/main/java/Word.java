public class Word implements Comparable<Word> {
    private String value;
    private int count;

    public Word(String value) {
        this.value = value;
    }

    public Word(String value, int count) {
        this.value = value;
        this.count = count;
    }


    public String getValue() {
        return this.value;
    }

    public int getWordCount() {
        return this.count;
    }

    @Override
    public int compareTo(Word input) {
        if (input.getWordCount() > this.count) {
            return 1;
        } else if (input.getWordCount() < this.count) {
            return -1;
        }
        return 0;
    }

    public String getFullString(){
        return this.value + " " + this.count;
    }
}
