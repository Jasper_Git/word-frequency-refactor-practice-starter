## Objective

- In the morning, the three groups in our class conducted a showcase of their respective design patterns teamwork, and I was tasked with analyzing the intention part and the question part of the presentation.
- In the afternoon, I learned how to refactor code and its intent, and how to find skills in code that need to be refactored, that is, code smell.

## Reflective

- After today, I have a deeper understanding of the three design patterns.
- Code refactoring is also a baby step process, which is addictive.

## Interpretive

- After the presentations of the three groups, I have a better understanding of the usage scenarios and purposes of the observer pattern, command mode, and policy pattern, not directly applied for the sake of use, but counterproductive, and need to be clear about the benefits to the code and the balance between maintenance costs.
- The purpose of code refactoring is to make the code better understood and reduce maintenance costs without changing the observable behavior of the software, not for performance optimization.

## Decisional

- I need to pay more attention and learn more code refactoring skills, practice more, and practice more.
- In addition to the design patterns covered in the coursework, I also need to learn other common design patterns, and in the process understand the full use of design patterns to the relationship between the three object-oriented features and classes.

## Problem

- Lack of proficiency in code refactoring, including some IDEA shortcuts, naming unfamiliar methods or variables, and some uncommon APIs in Stream that can concisely replace the original code